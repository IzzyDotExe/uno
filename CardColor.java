/*
  Israel Aristide - 213517

  CardColor.java - Enum
  Defines all colors of cards avalible.
*/

public enum CardColor {
  RED,
  BLUE,
  YELLOW,
  GREEN,
  BLACK
}
