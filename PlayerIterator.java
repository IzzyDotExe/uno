/*
  Israel Aristide - 213517

  PlayerIterator.java - Object
  PlayerIterator is a class that allows us to more easily manipulate the list of players
*/

public class PlayerIterator {

  private final Player[] _players;
  private int _index;
  private boolean _reverse;

  // The constructor for PlayerIterator will take in a list of players
  public PlayerIterator(Player[] players) {
    _players = players;
    _index = 0;
  }

  // The next() method will go forward in the iterator.
  public void next() {

    // Non reversed looping logic
    // if the current player index is equalto or greater than the player length - 1
    if (_index >= _players.length - 1 && !_reverse) {
      // make the current index, itself - the player length.
      _index = _index - _players.length;
    }
    // example: playerlen = 3, index = 2
    // if (2 >= [3 - 1] && !reverse) 
    //   index = 2 - 3
    // result: index = -1
    // index will be incremented at the end

    // reversed logic
    // if the current player index is less than or equal to zero
    // make the index the player length
    if (_index <= 0 && _reverse)
      _index = _players.length;

    // example: playerlen = 3 index = 0
    // if (0 <= 0 && reverse)
    //   index = 3
    // index will be decremented at the end
    
    // at the end, if reversed add -1 to the index if not reversed add 1
    _index += !_reverse ? 1 : -1;

  }
  
  // The reverse() method will flip the order that the iterator goes in.
  public void reverse() {
    _reverse = !_reverse;
  }

  // The get() method will get the current player that is selected by the iterator
  public Player get() {
    return _players[_index];
  }

  // viewNext allows us to view the next player without continuing the iterator.
  public Player viewNext() {

    // First run the next() method but do not apply it to the iterator
    // We will copy the current index of the iterator and then run it so that
    // we have a throwaway value of the next index would be.
    int fakeindex = _index;

    if (fakeindex >= _players.length - 1 && !_reverse)
      fakeindex = fakeindex - _players.length;

    if (fakeindex <= 0 && _reverse)
      fakeindex = _players.length; 

    fakeindex += !_reverse ? 1 : -1;

    // Return 
    return _players[fakeindex];

  }

  // viewPrev allows us to view the  previous player without continuing the iterator.
  public Player viewPrev() {

    // this will essentially run the next() method without making changes to our actual player index
    int fakeindex = _index;

    if (fakeindex >= _players.length - 1 && _reverse)
      fakeindex = fakeindex - _players.length;

    if (fakeindex <= 0 && !_reverse)
      fakeindex = _players.length;

    return _players[fakeindex + (_reverse ? 1 : -1)];
  }

}
