import java.util.Random;

/*
  Israel Aristide - 213517

  Deck.java - Object
  Defines the Draw Pile in UNO.
*/
public class Deck {

  // Holds all the cards avalible for use in the game.
  private CardsArray _cards;
  private final Random rnd = new Random();

  public Deck() {

    // This list will contain all the cards remaining in the draw pile
    _cards = new CardsArray();

    // This loop will add all the number cards in all colors from 0 - 9
    for (int i = 0; i <= Settings.NUMBER_CARD_MAXIMUM; i++) {

      // We are not looping through the color enum because it would be more lines of code to disclude the black color
      this._cards.add(new Card(CardColor.RED, CardType.NUMBER, i));
      this._cards.add(new Card(CardColor.GREEN, CardType.NUMBER, i));
      this._cards.add(new Card(CardColor.YELLOW, CardType.NUMBER, i));
      this._cards.add(new Card(CardColor.BLUE, CardType.NUMBER, i));

    }

    // This loop will add number cards again without the 0 cards because there are only 4 0 cards
    for (int i = 1; i <= Settings.NUMBER_CARD_MAXIMUM; i++) {
      this._cards.add(new Card(CardColor.RED, CardType.NUMBER, i));
      this._cards.add(new Card(CardColor.GREEN, CardType.NUMBER, i));
      this._cards.add(new Card(CardColor.YELLOW, CardType.NUMBER, i));
      this._cards.add(new Card(CardColor.BLUE, CardType.NUMBER, i));
    }

    // Special reverse, skip, draw2 cards will be added in this loop
    for (int i = 1; i <= Settings.SPECIAL_CARD_AMMOUNT; i++) {

      // reverse cards, again we arent looping through the color enum because there are no black skip cards and it would take more lines of code to disclude the black cards.
      this._cards.add(new Card(CardColor.RED, CardType.REVERSE, -11));
      this._cards.add(new Card(CardColor.GREEN, CardType.REVERSE, -11));
      this._cards.add(new Card(CardColor.BLUE, CardType.REVERSE, -11));
      this._cards.add(new Card(CardColor.YELLOW, CardType.REVERSE, -11));

      // Skip cards
      this._cards.add(new Card(CardColor.RED, CardType.SKIP, -10));
      this._cards.add(new Card(CardColor.GREEN, CardType.SKIP, -10));
      this._cards.add(new Card(CardColor.BLUE, CardType.SKIP, -10));
      this._cards.add(new Card(CardColor.YELLOW, CardType.SKIP, -10));

      // Draw cards
      this._cards.add(new Card(CardColor.RED, CardType.DRAW, Settings.DRAW_CARD_VALUE));
      this._cards.add(new Card(CardColor.GREEN, CardType.DRAW, Settings.DRAW_CARD_VALUE));
      this._cards.add(new Card(CardColor.BLUE, CardType.DRAW, Settings.DRAW_CARD_VALUE));
      this._cards.add(new Card(CardColor.YELLOW, CardType.DRAW, Settings.DRAW_CARD_VALUE));

    }

    // This final loop will add athe black wild cards to the deck.
    for (int i = 1; i <= Settings.WILD_CARD_AMMOUNT; i++) {

      this._cards.add(new Card(CardColor.BLACK, CardType.DRAW, Settings.WILD_DRAW_CARD_VALUE));
      this._cards.add(new Card(CardColor.BLACK, CardType.WILD, -1));
      
    }

  }

  // drawCard - Gets a random card from the drawpile. This essentially keeps the array shuffled at all times.
  public Card drawCard() {
    return this._cards.get(rnd.nextInt(this._cards.length()));
  }

  // getCard - gets a specific card from the deck using the index.
  public Card getCard(int index) {
    return this._cards.get(index);
  }

  // removeCard - Removes a specific card object from the deck.
  public void removeCard(Card obj) {
    this._cards.remove(obj);
  }

  // Returns the total ammount of cards.
  public int cardAmmount() {
    return this._cards.length();
  }

  // Reset method - Takes in a discard pile, empties the dicard pile and reshuffles the cards back into the draw pile.
  public void reset(Stack discardPile) {

    // We want to preserve the top card so that we can exclude it from the draw pile
    Card topcard = discardPile.getTopCard();
    Card[] discardCards = discardPile.getCards();

    // Delete everything in the discard pile except for the top card.
    discardPile.Clear();

    // For all the cards in the discard pile
    for (Card card : discardCards) {
      if (!(card.TYPE == CardType.WILD && card.COLOR != CardColor.BLACK))
        this._cards.add(card);
    }

    // Remove the top card from the draw pile
    this._cards.remove(topcard);

  }

  // DEBUG METHODS
  //
  // Used for developemnt purposes
  public void printDeck() {
    for (Card card : _cards.toArray()) {
      System.out.println(card);
    }
  }

}
