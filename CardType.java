/*
  Israel Aristide - 213517

  CardType.java - Enum
  Defines all types of cards avalible.
*/

public enum CardType {
  NUMBER,
  SKIP,
  REVERSE,
  DRAW,
  WILD
}
