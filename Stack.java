/*
  Israel Aristide - 213517

  Stack.java - Object
  Defines the Discard Pile in UNO.
*/
public class Stack {

  private CardsArray _cards;
  
  // Constructor
  public Stack() {
    // This list contains all the cards that are discarded.
    _cards = new CardsArray();  
  }

  // Addcard - Adds a Card to the top of the stack.
  public void AddCard(Card card) {
    this._cards.add(card);
  }

  // getTopCard - gets the card on the top of  the stack.
  public Card getTopCard() {
    return this._cards.get(this._cards.length() -1);
  }

  // getCards - gets the cards contained in the stack as a static array.
  public Card[] getCards() {
    return this._cards.toArray();
  }

  // Clear method - removes all cards from the discard pile except for the top card
  public void Clear() {

    int cardslen = _cards.length() -1;

    for (int i = 0; i < cardslen; i++)
      this._cards.remove(0);

  }


}
