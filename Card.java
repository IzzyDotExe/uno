/*
  Israel Aristide - 213517

  Card.java - Object
  Defines a single uno card.
*/
public class Card {

  // Final fields for the Card. These will never change.
  public final int VALUE;
  public final CardType TYPE;
  public final CardColor COLOR;

  // Other fields.
  // The effected field will be set to true if a card has already done its effect. 
  // So for example if someone places a +4 that wont affect the next player if they draw a card.
  private boolean _effected;

  // Constructor for default values.
  public Card(CardColor color, CardType type, int value) {
    this.COLOR = color;
    this.TYPE = type;
    this.VALUE = value;
    this._effected = false;
  }

  // Defines the object's string representation.
  public String toString() {

    // String representation
    // Example: <BLUE NUMBER Card; Value:7> : For cards with a value
    //          <RED SKIP CARD> : For cards with no value
    return "<" + COLOR + " " + TYPE + " Card" + (VALUE >= 0 ? "; Value: " + VALUE : (VALUE == Settings.DRAW_CARD_VALUE || VALUE == Settings.WILD_DRAW_CARD_VALUE ? "; Value: " + Math.abs(VALUE) : "")) + ">";
    
  }

  // get the effected field
  public boolean getEffected() {
    return this._effected;
  }

  // set the effected field
  public void setEffected(boolean value) {
    this._effected = value;
  } 

}
