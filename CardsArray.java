/*
    Israel Aristide - 2235617

    CardsArray.java - Object
    Defines the Draw Pile in UNO.
*/
public class CardsArray {
    
    // Holds all the items in the dynamic array.
    private Card[] _items;
    private int _length;

    // Constructor creates the internal static array and sets the _length to 0
    public CardsArray() {
        this._items = new Card[300];
        this._length = 0;
    }

    // Add Method - Adds a card to the dynamic array.
    public void add(Card card) {
        this._items[_length] = card;
        this._length ++;
    }

    // Length Method - Gets the ammount of items in the 
    public int length() {
        return this._length;
    }

    // Remove Method -- Deletes a card from the dynamic array based on the index
    public void remove(int index) {

        if (index >= this._length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index Out of Bounds.");
        }

        // create a new background static array.
        Card[] newItems = new Card[300];
        int newlen = 0;

        // while i is less than the total number of cards
        for (int i = 0, j = 0; i < this._length; i++) {
            // The length of our dynamic array will follow the value of i
            newlen = i;
            
            // if i is equal to the index we want to remove
            if (i == index) {

                // increment j
                j++;
                continue;
            }

            // at the index of i - j insert the current item at index i
            newItems[i - j] = this._items[i];
        }

        this._items = newItems;
        this._length = newlen;

    }

    // Remove Method -- Deletes a card from the dynamic array based on the specific object
    public void remove(Card card) {

        // get the index of the given card
        int index = indexOf(card);

        if (index < 0) {
            throw new IndexOutOfBoundsException("Card does not exist.");
        }

        // remove it
        remove(index);
    }

    // Get Method -- Gets a card from the dynamic array using its index.
    public Card get(int i) {

        if (i >= this._length) {
            throw new ArrayIndexOutOfBoundsException("Index Out of Bounds.");
        }

        return this._items[i];

    }

    // Index Of Method - Gets the index of a specific card object
    // Returns -1 if the card isnt in the array.
    // Also acts as a contains method
    public int indexOf(Card card) {
        
        // For every card in the array.
        for (int i = 0; i < this._length; i++) {
            
            // if the card is equal to the given input card return that index
            if (this._items[i].equals(card)) {
                return i;
            }
   
        }

        // If we dont find the card return -1 this way the indexof acts also as a contains method.
        return -1;
    }

    // ToArray method - converts the dynamic card array back to a static array
    public Card[] toArray() {
        
        Card[] staticArray = new Card[_length];

        // adds every card in the dynamic array into the static one
        for (int i = 0; i < _length; i++) {
            staticArray[i] = _items[i];
        }

        return staticArray;
    }

}
