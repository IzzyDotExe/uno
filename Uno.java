/*
  Israel Aristide - 213517

  Uno.java - Application Class
  Main class for running the program, also contains game logic.
*/

import java.util.Scanner;
import java.util.InputMismatchException;

public class Uno {

  public static void main(String args[]) {

    // This scanner will be used for all of our user input.
    Scanner input = new Scanner(System.in);

    // Create a List of all players.
    Player[] players = addPlayers(Settings.PLAYERS, input);
    // Create the player iterator
    PlayerIterator playerIterator = new PlayerIterator(players);

    // Create our draw and discard piles.
    Deck drawPile = new Deck();
    Stack discardPile = new Stack();

    // Initial game setup.
    init(drawPile, discardPile, players);

    boolean running = true;

    // Used to print a message whenever the user goes back to the main action menu
    String error = "";
    
    // Here Begins the main game loop.
    //
    while (running) { 

      // get the current top card
      Card topcard = discardPile.getTopCard();
      // get the current player
      Player currentPlayer = playerIterator.get();
      
      boolean turnover = false;

      if (!topcard.getEffected()) {
        cardEffect(currentPlayer, topcard, drawPile, playerIterator, discardPile, input);
        topcard = discardPile.getTopCard();
        currentPlayer = playerIterator.get();
      }

      printMainMenu(error, topcard, currentPlayer, playerIterator);

      // Accept user input
      String playerChoice = "";
      do {
        System.out.print("Type in your choice: ");
        playerChoice = input.next();
      } while (playerChoice.equals(""));
      
      // We will use a switch case to decide what happens when a user inputs their choice
      switch (Character.toUpperCase(playerChoice.charAt(0))) {
        // This option allows users to play a card.
        case 'P':

          // Select a card from the user's hand
          Card cardPlayed = selectCard(currentPlayer, input, topcard);

          // We now have to validate the card placed.
          // If the card is not a wild card, it has to be either:
          // 1. A number with the same color or value
          // 2. A draw, skip, or reverse card with the same color

          if (cardPlayed == null) {
            error = "Invalid card chosen, try again.";
            break;
          }

          // If the card color is not black
          if (cardPlayed.COLOR != CardColor.BLACK) {

            // check the color of the card
            if (cardPlayed.COLOR == topcard.COLOR) {
              // If the card is the same color we can just place it down.
              placeCard(discardPile, currentPlayer, cardPlayed);
              turnover = true;
              error = "";
              break;
            } else {
              // If it's not the same color we can check the value if its a number
              if (cardPlayed.TYPE == CardType.NUMBER) {
                if (cardPlayed.VALUE == topcard.VALUE) {
                  placeCard(discardPile, currentPlayer, cardPlayed);
                  turnover = true;
                  error = "";
                  break;
                }
              } else {
                if (cardPlayed.TYPE == topcard.TYPE && cardPlayed.TYPE != CardType.NUMBER) {
                  placeCard(discardPile, currentPlayer, cardPlayed);
                  turnover = true;
                  error = "";
                  break;
                }
              }
            }
          } else {

            placeCard(discardPile, currentPlayer, cardPlayed);
            turnover = true;
            error = "";
            break;
          }
          error = "That Card is not compatible!";
          break;

        case 'D':

          // Clears the terminal in supported shells
          System.out.println("\033[H\033[2J");

          // Draw a card from the draw pile
          Card card = drawPile.drawCard();
          drawPile.removeCard(card);
          currentPlayer.addCard(card);

          // Automaitcally reset uno to false when the player draws a card
          if (currentPlayer.isUno())
            currentPlayer.setUno(false);
            
          turnover = true;
          error = "";
          break;

        case 'H':

          // Clears the terminal in supported shells
          System.out.println("\033[H\033[2J");
          currentPlayer.printHand();
          System.out.println("Press any key and enter to continue...");
          input.next();
          error = "";
          break;

        case 'U':
          error = callUno(currentPlayer);
          break;
        case 'C':

          Player previous = playerIterator.viewPrev();
          // If the previous player has not called uno and has one card or if the player called uno and has more than one card give the previous player 2 cards
          // Otherwise give the accuser 2 cards
          if (!previous.isUno() && previous.cardAmmount() == 1 || previous.isUno() && !(previous.cardAmmount() == 1)) {
            error = "" + previous.getName() + " Didn't call uno or called a false uno! They get 2 cards";
            for (int i = 0; i < 2; i++) {
              Card carddraw = drawPile.drawCard();
              previous.addCard(carddraw);
              drawPile.removeCard(carddraw);
              previous.setUno(false);
            }
          } else {
            error = "" + currentPlayer.getName() + " falsely called out " + previous.getName() + "! So rude... They get 2 cards";
            for (int i = 0; i < 2; i++) {
              Card carddraw = drawPile.drawCard();
              currentPlayer.addCard(carddraw);
              drawPile.removeCard(carddraw);
            }
            turnover = true;
          }
          break;

        default:
          // If the user does not pick a valid option we will do nothing.
          System.out.println("\033[H\033[2J");
          System.out.println("");
          error = "Invalid Action! Try again...";
          System.out.println("");
          break;
      
      }

      // winCondition will return true if the win condition is met, we want to reverse the result because if the win condition is met we want to set running to false.
      running = !winCondition(currentPlayer);

      // If we have a low ammount of cards in the draw pile we will reset it by putting the discard pile cards back into the draw pile
      if (drawPile.cardAmmount() < 15) {
        drawPile.reset(discardPile);
      }
      
      // Continue to the next player
      if (turnover) {
        playerIterator.next();
      }
    
    }

  }

  // string callUno - toggles uno status for a player.
  //
  // Used inside of the main game loop. Returns a string confirming the user's uno toggle.
  private static String callUno(Player currentPlayer) {
    String error;
    // Clears the terminal in supported shells
    System.out.println("\033[H\033[2J");

    
    // If the user is already calling uno then set it to false.
    if (currentPlayer.isUno()) {
      currentPlayer.setUno(false);
      error = "You will not call uno when your turn is over.";
    }
    
    // If they are not already calling uno we can set it to true.
    else {
      currentPlayer.setUno(true);
      error = "You will now call uno when your turn is over.";
    }
    return error;
  }

  // boolean WinCondition - Evaluates the games win condition
  //
  // Used inside of the main game loop returns true if the given player won.
  private static boolean winCondition(Player currentPlayer) {
    // Win condition logic

    boolean WinConditionMet = false;

    // if the player has 0 cards set the win condition to true and print the win message
    if (currentPlayer.cardAmmount() == 0) {
      System.out.println("\033[H\033[2J");
      System.out.println("");
      System.out.println(currentPlayer.getName() + " WINS!!");
      WinConditionMet = true;
    }
    
    return WinConditionMet;

  }

  // void placeCard - takes the given card from the player and puts it on the top of the stack.
  //
  // Used inside of the main game loop returns nothing.
  private static void placeCard(Stack discardPile, Player currentPlayer, Card cardPlayed) {
    currentPlayer.removeCard(cardPlayed);
    discardPile.AddCard(cardPlayed);
  }

  // void printMainMenu - Prints the main UI of the game 
  //
  // USed inside of the main game loop returns nothing.
  private static void printMainMenu(String error, Card topcard, Player currentPlayer, PlayerIterator iterator) {
    // Clears the terminal (only in supported terminals)
    //
    System.out.println("\033[H\033[2J");
    System.out.println(error);

    // Print the player's availble choices if their turn hasnt been skipped
    System.out.println(currentPlayer.getName() + "'s turn!");
    System.out.println("The current card on the stack is " + topcard);
    System.out.println("------------------------------------");
    System.out.println("You have " + currentPlayer.cardAmmount() + " Cards.");
    System.out.println("Type in \'P\' to play a card.");
    System.out.println("Type in \'D\' to draw a card.");
    System.out.println("Type in \'U\' to toggle calling Uno.");
    System.out.println("Type in \'H\' to view your hand.");
    System.out.println("Type in \'C\' if " + iterator.viewPrev().getName() + " didn\'t call uno.");
    System.out.println("------------------------------------");
  }

  // Void cardEffect - Logic for the effects of the current card on the discard pile
  //                   Special cards like skips, reverse, and draw cards are done here.
  //
  // Used inside of the main game loop. Returns nothing.
  private static void cardEffect(Player currentPlayer, Card topcard, Deck drawPile, PlayerIterator iterator, Stack discardPile, Scanner input) {

    // Avalible special cards care:
    // Draw 2, Draw 4
    // Wild card
    // Skip
    // Reverse - Could be more difficult, maybe will skip this one


    // Draw Logic
    // Gives player cards equivalent to the VALUE field of the card to the current player, and skips their turn.
    if (topcard.TYPE == CardType.DRAW) {
      iterator.next();

      // loop as many times as the value of the draw card
      for (int i = 0; i < Math.abs(topcard.VALUE); i++) {

        // Give a card to the player
        Card drawCard = drawPile.drawCard();
        currentPlayer.addCard(drawCard);
        drawPile.removeCard(drawCard);

      }
    }

    // Skip logic
    // Skips the current player's turn
    if (topcard.TYPE == CardType.SKIP) {
      iterator.next();
    }
    

    // Wild card logic 
    // Brings up menu to change the color of the current card, since the color value is final, we will create a new instance of card with the new selected color.
    // This new card will act as the color of choice from the wild card picker
    // Since all black cards are wild cards in this version of uni we will check if the card is black. this has the added benifit of making it so we don't need
    // To have a specal case for the draw 4 card. The type of the draw 4 does not need to be wild because it is a black card.
    if (topcard.COLOR == CardColor.BLACK) {
      wildcardMenu(input, discardPile, topcard);
      System.out.println("\033[H\033[2J");
    }


    // Reverse card logic
    if (topcard.TYPE == CardType.REVERSE) {
      iterator.reverse();
      // since there is a next() at the end of the main game loop we want to immediately cancel it out here and then skip our own turn
      iterator.next();
      iterator.next();
    }
      
    // finally we will set the effected field so the card wont affect the game again.
    topcard.setEffected(true);
  }

  // Void wildCardMenu - Allows a user to select what color they want to switch to.
  //
  // Used inside of the main game loop returns nothing.
  private static void wildcardMenu(Scanner input, Stack discardPile, Card topcard) {

    // Print info and the prompt
    System.out.println("\033[H\033[2J");
    System.out.println("You Have placed a wild card! Here are the color options: ");
    System.out.println("");
    System.out.println("1. RED");
    System.out.println("2. BLUE");
    System.out.println("3. YELLOW");
    System.out.println("4. GREEN");
    System.out.println("");
    System.out.print("Select the color you would like to switch to: ");
    
    int playerChoice;
    
    try {
      playerChoice = input.nextInt();
    } catch (InputMismatchException err) {
      // set player choice to 0 so it will pick the default choice.
      playerChoice = 0;
    } 

    CardColor choicecolor;
    
    // Set the chosen color based on the input number that the user chose.
    switch (playerChoice) {
      case 1:
        choicecolor = CardColor.RED;
        break;
      case 2:
        choicecolor = CardColor.BLUE;
        break;
      case 3:
        choicecolor = CardColor.YELLOW;
        break;
      case 4:
        choicecolor = CardColor.GREEN;
        break;
      default:
        choicecolor = Settings.DEFAULT_WILD_CARD_COLOR;
        break;
    }

    // create a new card which is a wild card with the chosen color and add it to the discard pile.
    Card newCard = new Card(choicecolor, CardType.WILD, -1);
    discardPile.AddCard(newCard);
    topcard = newCard;

  } 


  // Card selectCard - Allows a user to select a card to play during their turn
  // 
  // Used inside of the main game loop. Returns the card that the user has chosen or null if the choice is invalid.
  private static Card selectCard(Player player, Scanner input, Card topcard) {
    
    // Print some info about the player hand and the prompt
    System.out.println("\033[H\033[2J");
    System.out.println("The current card on the stack is " + topcard);
    player.printHand();
    System.out.println("");
    System.out.print("Input the number of the card you want to play, if you no longer wish to play input any other number or letter: ");
    
    int playerChoice;

    // get the player choice, if it's invalid return null
    try {
      playerChoice = input.nextInt();
    } catch (InputMismatchException err) {
      input.next();
      return null;
    }

    // get the card that the player has chosen if the card doesnt exist return null
    try {
      Card card = player.getCard(playerChoice);
      return card;
    } catch (IndexOutOfBoundsException e) {
      return null;
    }
    
  }

  // Void init - Initial game setup.
  //
  // Used before the main game loop. Returns nothing.
  private static void init(Deck drawPile, Stack discardPile, Player[] players) {

    // Give cards to all the players
    distributeCards(drawPile, players);

    // We will grab a random card from the draw pile.
    Card topCardDraw = drawPile.drawCard();

    // set the effected property so that the first card wont effect the game.
    topCardDraw.setEffected(true);

    // Add to the discard pile and remove from the draw pile
    discardPile.AddCard(topCardDraw);
    drawPile.removeCard(topCardDraw);

  }

  // Player[] addPlayers - Creates all the players for our game
  //
  // Returns Player[] so we can use that list of players for our game loop
  private static Player[] addPlayers(int ammount, Scanner input) {

    // Create an array of players.
    Player[] playerlist = new Player[ammount];

    // Use a for loop to create each player in the array.
    for (int i = 0; i < ammount; i++) {
      System.out.print("Enter player " + (i+1) + "'s username: ");
      playerlist[i] = new Player(input.next());
    }
    
    return playerlist;

  }

  // Void distributeCards -- Adds 7 cards to all of the player's hands
  //
  // Used before the main game loop. Returns nothing
  private static void distributeCards(Deck drawPile, Player[] players) {
    
    // For every player in the player list
    for (Player player : players) {

      // Loop for the ammount of cards chosen in settings
      for (int i=0; i < Settings.CARDS_PER_PLAYER; i++) {

        // Draw a card from the draw pile
        Card drawCard = drawPile.drawCard();

        // Add the card to the players hand
        player.addCard(drawCard);
        drawPile.removeCard(drawCard);

      }

    }
    
  }

}
