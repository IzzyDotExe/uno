public class Settings {

  // Game Settings
  // 
  // Feel free to change these as you wish. Bugs may arise if you over do it. 
  // For Example if the starting cards is to high and we run out of cards to distribute.
  // 

  // Sets the ammount of cards each player will start out with - Recommended 7
  public static final int CARDS_PER_PLAYER = 2;

  // Sets the ammount of players. - Recommended 2 - 4
  public static final int PLAYERS = 4;


  // Advanced Settings
  //
  // More advanced constants for fine tuning. ONLY CHANGE THIS IF YOU KNOW WHAT YOU ARE DOING!!
  //
  
  // The defualt color chosen if in invalid value is given in the wild card menu
  public static final CardColor DEFAULT_WILD_CARD_COLOR = CardColor.RED;

  // The Ammount of cards given to a player from the draw cards and wild draw cards.
  // Defaults are 2 and 4 (MAKE SURE THE VALUES ARE IN NEGATIVE FOR THE GAME TO WORK CORRECTLY (because of code technicalities))
  public static final int DRAW_CARD_VALUE = -2;
  public static final int WILD_DRAW_CARD_VALUE = -4;

  // The max value for a given number card. Increasing this will cause the game to have more cards.
  // Default is 9, don't change this unless you want a weird game.
  public static final int NUMBER_CARD_MAXIMUM = 9;

  // The ammount of special cards that are added per color. default is 2
  public static final int SPECIAL_CARD_AMMOUNT = 2;

  // The ammount of each type of wild cards that are added. default is 4
  public static final int WILD_CARD_AMMOUNT = 4;

 
}
