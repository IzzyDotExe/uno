/*
  Israel Aristide - 213517

  Player.java - Object
  Defines the player
*/

public class Player {

  // Private fields 
  private String name;
  private boolean _isUno;
  private CardsArray _hand;

  // Constructor - Sets default values
  public Player(String name) {
    this.name = name;
    _isUno = false;
    _hand = new CardsArray();
  }

  // addCard - Adds a card to the user's hand
  public void addCard(Card card) {
    _hand.add(card);
  }

  // removeCard - removes a card from the user hand
  public void removeCard(Card card) {
    this._hand.remove(card);
  }

  // getCard - Gets a card from the users hand without removing it.
  public Card getCard(int index) {
    return _hand.get(index);
  }

  // cardAmmount - Gets the ammount of cards that a player has
  public int cardAmmount() {
    return this._hand.length();
  }

  // printhand - Shows the user's whole hand.
  public void printHand() {
    for (int i = 0; i < cardAmmount(); i++) {
      System.out.println(i+"."+" "+_hand.get(i));
    }
  }

  // get the uno field.
  public boolean isUno() {
    return this._isUno;
  }

  // set the uno field.
  public void setUno(boolean uno) {
    this._isUno = uno;
  }

  // get the username
  public String getName() {
    return name;
  }

}
